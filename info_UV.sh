#!/bin/bash

grep -e  "^\s[A-Z][A-Z][0-9][0-9]" -r  data/*.edt > donnees.txt
cut -d ':' -f 2-6 donnees.txt > donnees2.txt
cut -c 2- donnees2.txt > donnees3.txt

cut -c 1-4 donnees3.txt >donnees4.txt
uniq -c donnees4.txt > donnees5.txt

sort donnees5.txt | cut -c 8- > donnees6.txt

uniq  donnees6.txt > donnees7.txt

nb_uv=$(wc -l donnees7.txt | awk '{print $1}')


rm uv_nb_etu.txt
touch uv_nb_etu.txt
for i in `seq 1 $nb_uv` ; do
    uv_id=$(cat donnees7.txt | head -$i | tail -1)
    uv_nb_etu=$(grep $uv_id data/*.edt -m 1 | wc -l)
    echo $uv_id $uv_nb_etu >> uv_nb_etu.txt
done
uniq uv_nb_etu.txt

rm donnees?.txt
rm donnees.txt

#grep "SY02" data/api_init_projet_EdT/EdT/Data/*.edt -m 1 | wc -l






