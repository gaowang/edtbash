#!/bin/bash

# This script move *.edt files in ./data/backup
# Then mv back only the *.edt files whose login begins with a letter of your choice 

if [ ! -d "./data/backup" ]; then
    mkdir ./data/backup
    echo ./data/backup/ created
else
    echo backup folder exists
fi

echo "On which data (sub-)set do you want to work on ?"
echo "1) All"
echo "2) logins begin with letter 'a'"
echo "3) logins begin with other letter(s)"
echo "0) Quit"
echo "Nothing will be changed for all other inputs"
echo "--------------------------------------------"
echo "Your choice is ? "

read dev_choice

if [ $dev_choice == 3 ] ; then
    echo "Which letter do you want ?"
    read login_first_letter 
    while [[ $login_first_letter != [a-z] ]]; do
        echo "not a letter !! re-tape a letter pls"
        read new_letter
        login_first_letter=$new_letter
    done
fi

case $dev_choice in
    0) echo "nothing is changed ;-)" ; 
        echo Bonne programmation ;; 
    1) mv ./data/*.edt ./data/backup ; 
        mv ./data/backup/*.edt ./data ; 
        echo "Done : all *.edt are moved in ./data/" ;;
    2) mv ./data/*.edt ./data/backup ; 
        mv ./data/backup/a*.edt ./data/ ; 
        echo "Done : all a*.edt are moved in ./data/";;
    3) mv ./data/*.edt ./data/backup ; 
        mv ./data/backup/$login_first_letter*.edt ./data/ ; 
        echo "Done : all $login_first_letter*.edt are moved in ./data/ " ;;
    *) echo "votre choix n'existe pas" ;
        echo "-------------------------" ;
        echo "nothing is changed ;-)" ;;
esac
