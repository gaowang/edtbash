#!/bin/bash

echo 'Voici une liste des options'
echo '0) Quit'
echo '1) Trouver les logins qui correspondent à une entrée' 
echo '2) Liste des UVs pour un login donné'
echo "3) Entrer une branche et voir combien il y a d'etudiants dedans et lesquels"
echo '4) Salles utilisées pour une heure donnée'
echo '5) Salles utilisées pour un jour donné'
echo '6) Salles utilisées par une UV donnée'
echo '7) Liste des etudiants pour une UV donnée'
echo "8) nombre d'étudiants par UV"
echo "Quel est votre choix ?"
read choix

case $choix in
    0) echo have a nice day ;; 
    1) ./logEtu.sh;;
    2) ./UVetu.sh ;;
    3) ./BrancheEtu.sh ;;
    4) ./salle_heure.sh ;;
    5) ./salle_jour.sh ;;
    6) ./salle_uv.sh ;;
    7) ./listEtu_par_UV.sh ;;
    8) ./info_UV.sh;;
    *) echo "votre choix n'existe pas" && echo "-------------------------" && echo "Veuillez reselectioner une option : " ;;
esac
