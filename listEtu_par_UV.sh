#!/bin/bash

# let user to input the UV
# list all student of this UV

# The second line of *.edt files has the sufficiant info
# e.g. 
#  zhajingq                IM02       5  EN21  FQ01  LA92  MQ03  TN02  

# Créer une catalogue UV

awk 'FNR==2 {print $0}' ./data/*.edt > inscrit.tmptxt
awk '{$1=""; $2=""; $3=""; print $0}' inscrit.tmptxt > alluv.tmptxt
# awk '{printf("%s ", $0)}' alluv.tmptxt | awk '{for (i=1;i<=NF;i++) print $i}' | sort > catalogue_UV.tmptxt
awk '{printf("%s ", $0)}' alluv.tmptxt | awk '{for (i=1;i<=NF;i++) print $i}' | sort -u > catalogue_UV.tmptxt

# for a given user input, check if it is an UV
function f_exist_uv(){
    local exist
    for item in $(cat catalogue_UV.tmptxt);do
        if [ "$1" == "$item" ]; then
            echo you are good to go    
            # 0 = true exist
            return 0
        fi
    done
    return # 1 = false not exist
}

# Ask user to tape an UV code
echo "Which UV do you want to know about ?"
read choosedUV

# re-ask user if it is not an UV
while [ "0"==$(f_exist_uv $choosedUV) ] ; do
    if f_exist_uv $choosedUV ; then
        echo "UV non exsitante, veuillez re selectionner"
        read choosedUV
    fi
done

# For an valided UV code, list students
grep -hnr "$choosedUV" inscrit.tmptxt | awk '{print FILENAME, $0}' > $choosedUV.tmptxt
awk 'BEGIN{print "login   ","\tSemestre" ; 
                    print"------------------------------"}
                    {print $3,"\t",$4}' $choosedUV.tmptxt


# cleanups
rm *.tmptxt